"""
Return the Greatest Common Divisor of two integers
"""

# You can simply use math.gcd function
# or use one of the functions below

def gcd_non_recursive(n,m):
    if n == 0 or n == m:
        return m
    if m == 0:
        return n
    while n != 0 and m != 0:
        if n > m:
            n = n % m
        else:
            m = m % n
    return n if m == 0 else m

def gcd_recursive(n,m):
    if n == 0 or n == m:
        return m
    if m == 0:
        return n
    if n > m:
        return gcd(n%m, m)
    else:
        return gcd(n, m%n)
