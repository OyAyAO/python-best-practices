"""
Return the nth number in the Xbonacci Sequence
"""

def xbonacci_non_recursive(signature: list, n: int) -> int:
    res = signature[:n]
    for i in range(n - len(signature)): res.append(sum(res[-len(signature):]))
    return res[-1]

def xbonacci_recursive(signature: list, n: int) -> int: ### Not tested
    if n-1 < len(signature):
        return signature[n-1]
    return sum([Xbonacci_recursive(signature, n-i) for i in range(len(signature))])

"""
Return a list to the nth number in the Xbonacci Sequence
"""

def xbonacci_list_non_recursive(signature: list, n: int) -> int:
    res = signature[:n]
    for i in range(n - len(signature)): res.append(sum(res[-len(signature):]))
    return res

