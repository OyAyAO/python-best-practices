"""
Return the nth number in the Fibonacci Sequence
"""

def fibonacci_non_recursive(n: int, signature: list = [0, 1]) -> int:
    res = signature[:n+1]
    for i in range(n - 1): res.append(sum(res[-2:]))
    return res[-1]

def fibonacci_recursive(n: int, signature: list = [0, 1]) -> int:
    if n < len(signature):
        return signature[n]
    return fibonacci_recursive(n-1) + fibonacci_recursive(n-2)


"""
Return a list to the nth number in the Fibonacci Sequence
"""

def fibonacci_list_non_recursive(n: int, signature: list = [0, 1]) -> int:
    res = signature[:n+1]
    for i in range(n - 1): res.append(sum(res[-2:]))
    return res
