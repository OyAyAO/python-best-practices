"""
Return the nth number in the Tribonacci Sequence
"""

def tribonacci_non_recursive(signature: list, n: int) -> int:
    res = signature[:n]
    for i in range(n - 3): res.append(sum(res[-3:]))
    return res[n-1]

def tribonacci_recursive(signature: list, n: int) -> int:
    if n-1 < len(signature):
        return signature[n-1]
    return tribonacci_recursive(signature, n-1) + tribonacci_recursive(signature, n-2) + tribonacci_recursive(signature, n-3)

"""
Return a list to the nth number in the Tribonacci Sequence
"""

def tribonacci_list_non_recursive(signature: list, n: int) -> int:
    res = signature[:n]
    for i in range(n - 3): res.append(sum(res[-3:]))
    return res
