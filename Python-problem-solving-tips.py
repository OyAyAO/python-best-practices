#####################################################################
### Remove duplicates and maintain order of charcters in a string ###
#####################################################################
# Solution 1
result1 = list(dict.fromkeys(inp))

# Solution 2: count occurrence of characters
from collections import Counter
result2 = list(Counter(input).items())

####################################
### Alter each element of a list ###
####################################
# Solution 1
result1 = list(map(int, arr))

# Solution 2
result2 = [int(i) for i in arr]

#########################################
### Select specific element of a list ###
#########################################
# Solution 1
result1 = list(filter(lambda x: x>0, arr))

# Solution 2
result2 = [i for i in arr if i>0]

####################################
### Reverse the sign of a number ###
####################################
# Solution 1
result1 = -number

########################################################
### Replace a character in a string with another one ###
########################################################
# Solution 1: replace e with F and C with a
result1 = word.translate(str.maketrans("eC", "Fa"))

# Solution 2: replace any pattern that matches the regex-exp with D
from re import sub
result2 = sub("regex-exp", "D", word)

# Solution 3: 
result3 = word.replace('a' ,'b')

#########################################
### Reverse a list or string elements ###
#########################################
# Solution 1
result1 = word[::-1]

# Solution 2
result2 = "".join(reversed(word))

#######################################
### Char to ASCII and ASCII to char ###
#######################################
# Solution 1
"""
char contain a character
ascii contain ascii code a character
"""
result1 = ord(char)
result2 = chr(ascii)

###############################################
### Check if a string starts with something ###
###############################################
# Solution 1
"""
Check if word starts with '1' or '2' or '3'
"""
word.startswith(('1', '2', '3'))

####################################################
### Check if a string ends with a another string ###
####################################################
# Solution 1
result1 = word.endswith(tail)

###############################################################
### Change case from upper to lower and from lower to upper ###
###############################################################
# Solution 1
result1 = word.swapcase()

########################################
### Capitalize all words in a string ###
########################################
# Solution 1
result1 = word.capitalize()

########################################
### Execute string as a line of code ###
########################################
# Solution 1
a = 2
b = 3
operation = "+"
result1 = eval(f"{a}{operation}{b}") # result1 should be 2+3=5

#############################################
### Define a dict and get a value form it ###
#############################################
# Solution 1
default_value = "-"
result1 = {"a": 1, "b": 3, "operation": "+"}.get("operation", default_value)
"""
result1 should be "+"
or if 'operation' key doesn't exist, result1 should be "-" (the value of the variable default_value)
"""

################################
### Replace dict with string ###
################################
"""
Instead of defining a dict to compare things, create a string
"""
def game(p1, p2):
    wins = {
        "scissors": "paper",
        "paper": "rock",
        "rock": "scissors"
    }
    return 'Player 1 won!' if p2 == wins[p1] else 'Player 2 won!' if p1 == wins[p2] else 'Draw!'

def game(p1, p2):
    wins = "paper rock scissors paper"
    return "Player 1 won!" if f"{p1} {p2}" in wins else "Player 2 won!" if f"{p2} {p1}" in wins else "Draw!"

###############################################
### Loop through a list of lists vertically ###
###############################################
# Solution 1
result1 = ' '.join(map(''.join, zip(*arr)))
"""
arr = [
    ['J','L','L','M'],
    ['u','i','i','a'],
    ['s','v','f','n'],
    ['t','e','e','' ]
]
After looping, result1 will contain "Just Live Life Man"
"""

#############################################
### Sort a list according to custom logic ###
#############################################
# Solution 1
func = lambda x: 3.14 * x ** 2
result1 = sorted(cercles, key=func)
"""
cercles = [1.23, 3.444, 2.44, 4.6]
By default, sorted will sort cercles list from lowest to highest value
In this case, sorted will sort cercles list from lowest area to highest area
"""

####################################
### Check the type of a variable ###
####################################
# Solution 1
result1 = isinstance(word, str)
"""
Check if the word variable is of type str
"""

##############################
### Reverse sort of a list ###
##############################
# Solution 1
result1 = sorted(arr, reverse=True)
"""
Sort tab from highest to lowest value 
"""

##############################################
### Find the closest number multiple of 10 ###
##############################################
# Solution 1
result1 = round(i, -1)

#############################################################
### Multiple features from different classes in one class ###
#############################################################
# Solution 1
from collections import OrderedDict, Counter
class OrderedCounter(Counter, OrderedDict):
    pass
result1 = list(OrderedCounter(word).items())
"""
In this case, create class to inherite from Counter and OrderedDict classes.
The goal is to count occurrences while maintaining characters' appearing order
"""

########################
### Unpacking a list ###
########################
# Solution 1: unpack arr to convert it to a set
result1 = {*arr}

######################
### Flatten a list ###
######################
"""
arr = [
    [78], 
    [117, 110, 99], 
    [104, 117], 
    [107, 115]
]
result should be [78, 117, 110, 99, 104, 117, 107, 115]
"""
# Solution 1
from itertools import chain
result1 = list(map(lambda x: x, chain(*arr)))

# Solution 2
result2 = [x for sublist in arr for x in sublist]

###########################
### Compare two numbers ###
###########################
# Solution 1:
result1 = cmp(nbr1, nbr2)

#####################################
### Compare two softwares version ###
#####################################
# Solution 1:
from packaging.version import parse
result1 = parse(version1) >= parse(version2)

################################################
### Center a word in a string between spaces ###
################################################
# Solution 1: 
l = 5 #total length of result1
result1 = word.center(l)

##################################################################
### Delete whitespaces from string on the left or on the right ###
##################################################################
# Solution 1: trim the string from the right
result1 = word.rstrip()
# Solution 2: trim the string from the left
result1 = word.lstrip()

##################################################################
### Compare an element of a list with the rest of the elements ###
##################################################################
# Solution 1: 
from itertools import combinations
for a, b in combinations(arr, 2):
    print(num1)
    print(num2)

##########################
### Read data from XML ###
##########################
# Solution 1: Read from a string
"""
xml_string = "<prod><name>drill</name><prx>99</prx><qty>5</qty></prod>"
"""
import xml.etree.ElementTree as ET
tree = ET.fromstring(xml_string)
name = tree[0].text
prx = tree[1].text
qty = tree[2].text

